> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Assignment #4 Requirements:

#### Assignment Screenshots:

*Screenshot of Mortgage Interest Calculator Splash Page*:

![Mortgage Interest Calculator Splash Page](img/splash.png "Mortgage Interest Calculator Splash Page")

*Screenshot of Mortgage Interest Calculator Error*:

![Mortgage Interest Calculator Error Screenshot](img/error.png "Mortgage Interest Calculator Error")

*Screenshot of Mortgage Interest Calculator Computed Interest*:

![Mortgage Interest Calculator Computed Interest Screenshot](img/output.png "Mortgage Interest Calculator Computed Interest")

