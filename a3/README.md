> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Assignment #3 Requirements:

#### Assignment Screenshots:

*Screenshot of Money Converter Unpopulated*:

![Money Converter Unpopulated Screenshot](img/unpop.png "Money Converter UnPopulated")

*Screenshot of Money Converter Populated*:

![Money Converter Populated Screenshot](img/pop.png "Money Converter Populated")

*Screenshot of Money Converter Toast Message*:

![Money Converter Populated Screenshot](img/toast.png "Money Converter Toast Message")

