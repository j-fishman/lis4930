> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Assignment #2 Requirements:

#### Assignment Screenshots:

*Screenshot of Tip Calculator Unpopulated*:

![Tip Calculator Unpopulated Screenshot](img/unpopulated.jpg "Tip Calculator UnPopulated")

*Screenshot of Tip Calculator Populated*:

![Tip Calculator Populated Screenshot](img/populated.jpg "Tip Calculator Populated")

