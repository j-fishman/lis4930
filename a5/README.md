> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Assignment #5 Requirements:

#### Assignment Screenshots:

*Screenshot of Items Activity*:

![Items Activity](img/activity.png "Items Activity")

*Screenshot of Item Activity*:

![Item Activity Screenshot](img/item.png "Item Activity")

*Screenshot of US News by Read More link*:

![US News by Read More link Screenshot](img/read.png "US News by Read More link")

