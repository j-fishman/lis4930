> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Project 1 Requirements:

#### Assignment Screenshots:

*Screenshot of My Music Follow-Up Screen*:

![My Music Splash Screenshot](img/splash.png "My Music Splash Screen")

*Screenshow of My Music Follow-Up Screen*:

![My Music Follow-Up Screenshot](img/followup.png "My Music Follow-Up Screen")

*Screenshot of My Music Playing Screen*:

![My Music Playing Screen Screenshot](img/play.png "My Music Playing Screen")

*Screenshot of My Music Paused Screen*:

![My Music Paused Screen Screenshot](img/pause.png "My Music Paused Screen")

