> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Class Assignments


1.[A1 README](https://bitbucket.org/j-fishman/lis4930/raw/master/a1/ "A1 README")

 - Screenshot of running java Hello.
 - Screenshot of running Android Studio - My First App 
 - git commands w/short descriptions
 - Bitbucket repo links a) this assignment and b)the completed tutorials above (bitbucketstationlocations and myteamquotes)

2.[A2 README.md](a2/README.md "My A2 README.md file")

 - Bill Application
 - Calculates the cost per person for a combined meal price
 - Takes into consideration the amount of people and tip percentage

3.[A3 README.md](a3/README.md "My A3 README.md file")

- Money Converter Application
- Uses Toast Messages
- Converts US Dollars to Euros, Pesos, and Canadian Dollars

4.[P1 README.md](p1/README.md "My P1 README.md file")

- My Music Application
- Uses Splash Screen
- Plays three songs using Media Player and Button Controls

5.[A4 README.md](a4/README.md "My A4 README.md file")

- Mortgage Interest Calculator Application
- Uses Shared Preferences
- Calculates Interest based on number of Years, Monthly payments, and Principal

6.[A5 README.md](a5/README.md "My A5 README.md file")

- News Reader Application
- Uses RSS feed to compile a list of news articles 
- Includes working link to specific articles 
- Source is US News - Money

7.[P2 README.md](p2/README.md "My P2 README.md file")

- Task List Application
- Uses SQLite
- Completed with Databases for Tasks


...
