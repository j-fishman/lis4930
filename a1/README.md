> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Web Application

## Jenna Fishman

### Assignment #1 Requirements:

*Three parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of running java Hello.
* Screenshot of running Android Studio - My First App
* Screenshot of running Android Application
* git commands w/short descriptions
* Bitbucket repo links a) this assignment and b)the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - Lists any files that have changes and those needed to add or commit
3. git add - Adds file(s) to index
4. git commit - Commits changes to head but not repository
5. git push - Sends changes to master branch 
6. git pull - Fetches and merges changes
7. git checkout - Switches from one branch to another

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](https://bitbucket.org/j-fishman/lis4381/raw/master/a1/images/jdkinstall.png "JDK Screenshot")

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](https://bitbucket.org/j-fishman/lis4381/raw/master/a1/images/android.png "Android Screenshot")

*Screenshot of Android Application*:

![Android Studio Application Screenshot](https://bitbucket.org/j-fishman/lis4930/raw/master/a1/images/me.png "Android Application Screenshot")

*Screenshot of Android Application*:

![Android Studio Appplication Screenshot](https://bitbucket.org/j-fishman/lis4930/raw/master/a1/images/android2.png "Android Application Screenshot")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/j-fishman/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/j-fishman/myteamquotes/ "My Team Quotes Tutorial")
